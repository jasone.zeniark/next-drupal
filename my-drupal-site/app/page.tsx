import ExpertiseSection from "@/components/homepage/ExpertiseSection"
import HeroBanner from "@/components/homepage/HeroBanner"
import ServiceSection from "@/components/homepage/ServicesSection"
import { drupal } from "@/lib/drupal"
import { Service } from "@/types/service"
import type { Metadata } from "next"
import type { DrupalNode, DrupalView } from "next-drupal"
import { revalidatePath } from "next/cache"

export const metadata: Metadata = {
  description: "A Next.js site powered by a Drupal backend.",
}

export default async function Home() {
  const [homepage] = await drupal.getResourceCollection<DrupalNode[]>(
    "node--home_page",
    {
      params: {
        // "fields[paragraph--service_paragraph]": "field_service_content",
        include: "field_services_list.field_service_link,field_expertise",
      },
    }
  )
  console.log({ serviceView: homepage.field_service_view })
  console.log({ serviceList: homepage.field_services_list })

  return (
    <>
      <HeroBanner title={homepage.field_header_banner_title} />
      <ServiceSection
        services={homepage.field_service_view as Service[]}
        title={homepage.field_second_banner_title}
      />
      <ExpertiseSection expertiseList={homepage.field_expertise} />
    </>
  )
}
