import { title } from "process"

interface HeroBannerProps {
  title: string
}

export default function HeroBanner({ title }: HeroBannerProps) {
  return (
    <div>
      <h1 className="font-bold text-4xl text-center">{title}</h1>
    </div>
  )
}
