"use client"
import { Expertise } from "@/types/expertise"
import { useState } from "react"

interface ExpertiseSectionProps {
  expertiseList: Expertise[]
}

export default function ExpertiseSection({
  expertiseList,
}: ExpertiseSectionProps) {
  const [details, setDetails] = useState<
    Pick<Expertise, "field_expertise_description" | "field_expertise_title">
  >(expertiseList[0])

  return (
    <section className="flex items-stretch my-20 gap-3">
      <ul>
        {expertiseList.map((item) => (
          <li
            key={item.id}
            className={`my-2 hover:cursor-pointer hover:text-blue-600 transition-all ${details.field_expertise_title === item.field_expertise_title ? "text-blue-600 underline" : ""}`}
            onClick={() => setDetails({ ...item })}
          >
            {item.field_expertise_title}
          </li>
        ))}
      </ul>
      <div className="bg-[#0693e3] p-4 flex-1 text-white">
        <h2 className="font-bold text-lg">{details.field_expertise_title}</h2>
        <p className="mt-4">{details.field_expertise_description}</p>
      </div>
    </section>
  )
}
