"use client"
import { Service } from "@/types/service"
import { revalidatePath } from "next/cache"
import Link from "next/link"

interface ServiceProps {
  services: Service[]
  title: string
}
export default function ServiceSection({ services, title }: ServiceProps) {
  console.log(services)
  return (
    <section className="mt-10">
      <h2 className="font-semibold text-3xl text-center mb-5">{title}</h2>
      <div className="flex items-stretch justify-center flex-wrap">
        {services.map((item) => (
          <div
            key={item.id}
            className="bg-[#0693e3] p-5 m-2 text-white w-[18rem] flex items-start justify-between flex-col"
          >
            <div className="">
              <h2 className="font-bold text-xl">{item.field_service_title}</h2>
              <div dangerouslySetInnerHTML={{ __html: item.body.value }} />
            </div>
            <Link
              className="mt-3 hover:font-bold transition-all hover:underline"
              href={item.path?.alias ?? ""}
            >
              Read more
            </Link>
          </div>
        ))}
      </div>
    </section>
  )
}
