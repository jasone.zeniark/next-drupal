import { Service } from "@/types/service"
import { DrupalNode } from "next-drupal"

interface ServicePageProps extends DrupalNode {}

export default function ServicePage({
  field_service_title,
  body,
  ...props
}: ServicePageProps & Service) {
  return (
    <article>
      <h1 className="font-bold text-4xl">{field_service_title}</h1>
      <div className="mt-4" dangerouslySetInnerHTML={{ __html: body.value }} />
    </article>
  )
}
