import { DrupalParagraph } from "next-drupal"

export interface Service extends DrupalParagraph {
  field_service_content: string
  field_service_title: string
  paragraph_type: {}
  field_service_link: {
    path: { alias: string; pid: number; langcode: string }
  }
  body: {
    value: string
    format: string
    processed: string
    summary: string
  }
  path: { alias: string; pid: number; langcode: string }
}
