import { DrupalParagraph } from "next-drupal"

export interface Expertise extends DrupalParagraph {
  type: string
  parent_field_name: string
  field_expertise_description: string
  field_expertise_title: string
}
